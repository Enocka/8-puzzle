import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;

import java.util.Iterator;

public class Solver {
    private boolean isSolvable = false;
    private Node currentNode;
    private Node firstNode;
    private int movesMade = 0;

    private class Node implements Comparable<Node>{
        Board board;
        Node predecessorNode;
        int movesMade;
        public Node(Board board, Node predecessorNode, int movesMade){
            this.board = board;
            this.predecessorNode = predecessorNode;
            this.movesMade = movesMade;
        }

        public int compareTo(Node that){
            if ((this.board.manhattan() + this.movesMade) > (that.board.manhattan() + that.movesMade))
                return 1;
            if ((this.board.manhattan() + this.movesMade) < (that.board.manhattan() + that.movesMade))
                return -1;
            return 0;
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial){
        //solve two boards in lockstep to search for the unsolvable one
        MinPQ<Node> solverPQ = new MinPQ<Node>();
        MinPQ<Node> twinSolverPQ = new MinPQ<Node>();

        int gameTreeDepth = 0;
        int twinGameTreeDepth = 0;
        Board twin = initial.twin();
        firstNode = new Node(initial, null, 0);
        solverPQ.insert(new Node(initial, null, 0));
        twinSolverPQ.insert(new Node(twin, null, 0));
        int nextTurn = 1;
        while(solverPQ.size() > 0 || twinSolverPQ.size() > 0){
            if (nextTurn == 1){
                currentNode = solverPQ.delMin();
                if (currentNode.board.isGoal()){
                    System.out.println("at hii ndio solution, after moves "+currentNode.movesMade);
                    System.out.println(currentNode.board.toString());
                    movesMade = currentNode.movesMade;
                    isSolvable = true;
                    break;
                }
                System.out.println("toa hii");
                System.out.println(currentNode.board.toString());
                Iterable<Board> neighbors = currentNode.board.neighbors();
                gameTreeDepth++;
                for (Board neighbor : neighbors) {
                    System.out.println("weka hizi, zeney zikona moves "+gameTreeDepth);
                    System.out.println(neighbor.toString());
                    System.out.println(currentNode.predecessorNode);
                    if (currentNode.predecessorNode != null && neighbor.equals(currentNode.predecessorNode.board)) continue;
                    solverPQ.insert(new Node(neighbor, currentNode, gameTreeDepth));
                }
                nextTurn = 2;
                continue;
            }
            currentNode = twinSolverPQ.delMin();
            if (currentNode.board.isGoal())
                break;
            Iterable<Board> neighbors = currentNode.board.neighbors();
            twinGameTreeDepth++;
            for (Board neighbor : neighbors) {
                if (currentNode.predecessorNode != null && neighbor.equals(currentNode.predecessorNode.board)) continue;
                twinSolverPQ.insert(new Node(neighbor, currentNode, twinGameTreeDepth));
            }
            nextTurn = 1;
        }
    }

    // is the initial board solvable?
    public boolean isSolvable(){
        return isSolvable;
    }

    // min number of moves to solve initial board; -1 if unsolvable
    public int moves(){
        if (isSolvable) return movesMade;
        return -1;
    }

    // sequence of boards in a shortest solution; null if unsolvable
    public Iterable<Board> solution(){
        if (!isSolvable) return null;
        Queue<Board> solution = new Queue<>();
        solution.enqueue(firstNode.board);
        while(currentNode.movesMade != 0){
            solution.enqueue(currentNode.board);
            currentNode = currentNode.predecessorNode;
        }
//        solution.enqueue(currentNode.board);
        return solution;
    }

    // solve a slider puzzle
    public static void main(String[] args){
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        // solve the puzzle
        System.out.println("tumefika hapa");
        Solver solver = new Solver(initial);
        Iterator<Board> solutionIterator = solver.solution().iterator();
        System.out.println("tumepata solver"+ solver);
        while(solutionIterator.hasNext()){
            System.out.println(solutionIterator.next().toString());
        }
        System.out.println("na number of moves ni "+solver.moves());
        // print solution to standard output
//        if (!solver.isSolvable())
//            StdOut.println("No solution possible");
//        else {
//            StdOut.println("Minimum number of moves = " + solver.moves());
//            for (Board board : solver.solution().iterator())
//                StdOut.println(board);
//        }
    }
}