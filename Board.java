import java.lang.Math;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.Queue;

public class Board {
    private int[][] blocks;
    private int dimension;

    // construct a board from an n-by-n array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks){
        this.blocks = blocks;
        dimension = dimension();
    }

    // board dimension n
    public int dimension(){
        return blocks[0].length;
    }

    // number of blocks out of place
    public int hamming(){
        int hamming = 0;
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension; j++){
                if(blocks[i][j] == 0)
                    continue;
                if(blocks[i][j] != ((i * dimension) + j + 1))
                    hamming++;
            }
        }
        return hamming;
    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan(){
        int manhattan = 0;
        for(int i = 0; i < dimension; i++){
            for(int j = 0; j < dimension; j++){
                if(blocks[i][j] == 0)
                    continue;
                if(blocks[i][j] == ((i * dimension) + j + 1))
                    continue;
                int foundNumber = blocks[i][j];
                int foundNumberIndex = foundNumber - 1;
                int foundNumberIndexX = (foundNumberIndex%dimension);
                int foundNumberIndexY = foundNumberIndex/dimension;
                int yDisplacement = Math.abs(foundNumberIndexY - i);
                int xDisplacement = Math.abs(foundNumberIndexX - j);
                manhattan = manhattan + yDisplacement + xDisplacement;
            }
        }
        return manhattan;
    }

    // is this board the goal board?
    public boolean isGoal(){
        for(int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension - 1; j++) {
                if (blocks[i][j] != ((i * dimension) + j + 1))
                    return false;
            }
        }
        return true;
    }

//    private flatten(){
//        int filledPositionOne = (emptyPosition + 1) % (dimension * dimension);
//        int filledPositionTwo = filledPositionOne + 1;
//        int filledPositionOneY = filledPositionOne/dimension;
//        int filledPosiiontOneX = filledPositionOne - (filledPositionOneY * dimension);
//        int filledPositionTwoY = filledPositionTwo/dimension;
//    }

    // a board that is obtained by exchanging any pair of blocks
    public Board twin(){
        int emptyPosition = (dimension*dimension) - 1;
        int [][] blocksCopy = blocks;
        for(int i = 0; i < dimension; i++){
            for (int j = 0; j < dimension; j++){
                if(blocks[i][j] == 0)
                    emptyPosition = (i*dimension) + j;
            }
        }
        int filledPositionOne = (emptyPosition + 1) % (dimension * dimension);
        int filledPositionTwo = filledPositionOne + 1;
        int filledPositionOneY = filledPositionOne/dimension;
        int filledPosiiontOneX = filledPositionOne - (filledPositionOneY * dimension);
        int filledPositionTwoY = filledPositionTwo/dimension;
        int filledPositionTwoX = (filledPositionTwo%dimension == 0 ? dimension : filledPositionTwo%dimension);
        int temp = blocks[filledPosiiontOneX][filledPositionOneY];
        blocksCopy[filledPosiiontOneX][filledPositionOneY] = blocksCopy[filledPositionTwoX][filledPositionTwoY];
        blocksCopy[filledPositionTwoX][filledPositionTwoY] = temp;
        return new Board(blocksCopy);
    }

    // does this board equal y?
    public boolean equals(Object y){
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass())
            return false;

        Board that = (Board)y;
        for (int i=0; i < dimension; i++){
            for (int j=0; j < dimension; j++){
                if(this.blocks[i][j] != that.blocks[i][j])
                    return false;
            }
        }
        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors(){
        System.out.println("neighbours imecalliwa");
        int emptyPositionX = dimension - 1;
        int emptyPositionY = dimension - 1;
        int [][] blocksCopy = new int[dimension][dimension];
        Queue<Board> neighbours = new Queue<Board>();
        for(int i = 0; i < dimension; i++){
            for (int j = 0; j < dimension; j++){
                if(blocks[i][j] == 0){

                    emptyPositionX = j;
                    emptyPositionY = i;
                    System.out.println("emptyPosition imepatikana "+emptyPositionX+","+emptyPositionY);
                }
            }
        }

        //resultant board with empty moved left
        if(emptyPositionX != 0){
//            blocksCopy = blocks;
            int [][] blocksCopyLefted = new int[dimension][dimension];
            for(int i = 0; i < dimension; i++){
                for (int j = 0; j < dimension; j++){
                    blocksCopyLefted[i][j] = blocks[i][j];
                }
            }
            blocksCopyLefted[emptyPositionY][emptyPositionX] = blocksCopyLefted[emptyPositionY][emptyPositionX-1];
            blocksCopyLefted[emptyPositionX-1][emptyPositionY] = 0;
            Board lefted = new Board(blocksCopyLefted);
            neighbours.enqueue(lefted);
        }

        //resultant board with empty moved right
        if(emptyPositionX != dimension - 1){
            //            blocksCopy = blocks;
            int [][] blocksCopyRighted = new int[dimension][dimension];
            for(int i = 0; i < dimension; i++){
                for (int j = 0; j < dimension; j++){
                    blocksCopyRighted[i][j] = blocks[i][j];
                }
            }
            blocksCopyRighted[emptyPositionY][emptyPositionX] = blocksCopyRighted[emptyPositionY][emptyPositionX+1];
            blocksCopyRighted[emptyPositionY][emptyPositionX+1] = 0;
            Board righted = new Board(blocksCopyRighted);
            neighbours.enqueue(righted);
        }

        //resultant board with empty moved down
        if(emptyPositionY != dimension - 1){
            //            blocksCopy = blocks;
            int [][] blocksCopyDowned = new int[dimension][dimension];
            for(int i = 0; i < dimension; i++){
                for (int j = 0; j < dimension; j++){
                    blocksCopyDowned[i][j] = blocks[i][j];
                }
            }
            blocksCopyDowned[emptyPositionY][emptyPositionX] = blocksCopyDowned[emptyPositionY + 1][emptyPositionX];
            blocksCopyDowned [emptyPositionY + 1][emptyPositionX] = 0;
            Board downed = new Board(blocksCopyDowned);
            neighbours.enqueue(downed);
        }

        //resultant board with empty moved up
        if(emptyPositionY != 0){
            //            blocksCopy = blocks;
            int [][] blocksCopyUpped = new int[dimension][dimension];
            for(int i = 0; i < dimension; i++){
                for (int j = 0; j < dimension; j++){
                    blocksCopyUpped[i][j] = blocks[i][j];
                }
            }
            blocksCopyUpped[emptyPositionY][emptyPositionX] = blocksCopyUpped[emptyPositionY - 1][emptyPositionX];
            blocksCopyUpped[emptyPositionY - 1][emptyPositionX] = 0;
            Board upped = new Board(blocksCopyUpped);
            neighbours.enqueue(upped);
        }
        return neighbours;
    }

    // string representation of this board (in the output format specified below)
    public String toString(){
        StringBuilder s = new StringBuilder();
        s.append(dimension + "\n");
        for(int i = 0; i < dimension; i++){
            for (int j = 0; j < dimension; j++){
                s.append(String.format("%2d ", blocks[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    // unit tests (not graded)
    public static void main(String[] args){
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int x = in.readInt();
                blocks[i][j] = x;
            }
        }
        System.out.println("buda ni nini");
        Board board = new Board(blocks);
        for( Board b : board.neighbors() ){
            System.out.println(b.toString());
        }
    }
}